insert into client(first_name, last_name, age)
values ('Марсель', 'СидиковАБВ', 14);
insert into client(first_name, last_name, age)
values ('Иванов', 'Яков', 18);
insert into client(first_name, last_name, age)
values ('Судаков', 'Дмитрий', 19);
insert into client(first_name, last_name, age)
values ('Сабитов', 'Айнур', 18);

update client
set last_name = 'Сидиков',
    age          = 24
where id = 1;

insert into driver (first_name, last_name, rating, age)
values ('Захириддин', 'Умаров', 4.2, 32),
       ('Джамшид', 'Нурматов', 2.3, 18),
       ('Абубакар', 'Айвазян', 5.0, 54);

update driver
set phone_number = '+7 9005553535'
where id = 1;

insert into car (model, year_of_issue, color, maximum_speed)
values ('ВАЗ 2107', '1982-03-11', 'red', 290),
       ('BMW i8', '2014-01-01', 'black', 160),
       ('Москвич 2022', '2023-01-01', 'white', 410);

update car
set color = 'green',
    maximum_speed          = 292
where id = 1;

insert into command (order_date, trip_length, driver_id, client_id, car_id)
values ('2021-03-11', 11, 2, 3, 3),
       ('2022-04-15', 123, 1, 2, 1),
       ('2022-05-21', 23, 3, 2, 1);

drop table if exists client;
drop table if exists driver;
drop table if exists car;
drop table if exists command;

create table client
(
    id         bigserial primary key,
    first_name char(30),
    last_name  char(30),
    age        integer check ( age > 13 and age < 120)
);

create table driver
(
    id         bigserial primary key,
    first_name char(30),
    last_name  char(30),
    rating real,
    age        integer check ( age > 17 and age < 70)
);


create table car
(
    id serial primary key,
    model char(40),
    year_of_issue date,
    color char(30),
    maximum_speed integer
);

create table command
(
    id         bigserial primary key,
    order_date date,
    trip_length integer
);

alter table driver add phone_number char(20) not null default '';

alter table command add driver_id bigint;
alter table command add foreign key(driver_id) references driver(id);
alter table command add client_id bigint;
alter table command add foreign key(client_id) references client(id);
alter table command add car_id bigint;
alter table command add foreign key(car_id) references car(id);

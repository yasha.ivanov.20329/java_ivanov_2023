-- 1 --
SELECT Product.model, PC.speed, PC.hd
FROM Product
JOIN PC ON PC.model = Product.model
WHERE PC.price < 500

-- 2 --
SELECT DISTINCT maker 
FROM Product  
WHERE type = 'Printer'

-- 3 --
SELECT Product.model, Laptop.ram, Laptop.screen
FROM Product
JOIN Laptop ON Laptop.model = Product.model
WHERE Laptop.price > 1000

-- 4 --
SELECT *
FROM Printer
WHERE Printer.color = 'y'

-- 5 --
SELECT PC.model, PC.speed, PC.hd
FROM PC
WHERE (PC.cd = '12x' OR PC.cd = '24x') AND (PC.price < 600)

-- 6 --
SELECT DISTINCT Product.maker, Laptop.speed
FROM Product
JOIN Laptop ON Laptop.model = Product.model
WHERE Laptop.hd >= 10

-- 7 --
SELECT DISTINCT Product.model, pc.price
FROM Product
JOIN PC on Product.model = PC.model
WHERE maker = 'B'
UNION SELECT DISTINCT Product.model, Laptop.price
FROM Product 
JOIN Laptop on Product.model = Laptop.model
WHERE maker = 'B'
UNION SELECT DISTINCT Product.model, Printer.price
FROM Product
JOIN Printer on Product.model = Printer.model
WHERE maker = 'B'

-- 8 --
SELECT maker
FROM Product
WHERE type = 'pc'
EXCEPT SELECT maker
FROM product
WHERE type = 'laptop'

-- 9 --
SELECT DISTINCT maker
FROM Product
JOIN PC ON PC.model = Product.model
WHERE PC.speed >= 450

-- 10 --
SELECT DISTINCT model, price
FROM Printer
WHERE price =
(SELECT MAX(price) FROM Printer)

-- 11 --
SELECT AVG(speed)
FROM PC

-- 12 --
SELECT AVG(speed)
FROM Laptop
WHERE price > 1000

-- 13 --
SELECT AVG(PC.speed)
FROM PC
JOIN Product on PC.model = Product.model
WHERE maker = 'A'

-- 14 --
SELECT Ships.class, Ships.name, Classes.country
FROM Classes
JOIN Ships on Classes.class = Ships.class
WHERE numguns >= 10

-- 15 --
SELECT PC.hd
FROM PC
GROUP BY PC.hd HAVING count(model) >= 2

-- 18 --
SELECT DISTINCT Product.maker, Printer.price
FROM Product
JOIN Printer on Product.model = Printer.model
WHERE Printer.price = (SELECT MIN(price)
FROM Printer
WHERE color = 'y') AND Printer.color = 'y'

-- 19 --
SELECT Product.maker, AVG(Laptop.screen)
FROM Product
JOIN Laptop on Product.model = Laptop.model
GROUP BY Product.maker

-- 20 --
SELECT maker, count(model)
FROM Product
WHERE type = 'pc'
GROUP BY maker HAVING COUNT(model) >= 3

-- 21 --
Select Product.maker, MAX(Pc.price)
FROM Product
JOIN Pc on Product.model = Pc.model
GROUP BY maker

-- 22 --
SELECT speed, avg(price)
FROM Pc
WHERE speed > '600'
GROUP BY speed

-- 23 --
SELECT Product.maker
FROM Product
JOIN pc pc on Product.model = pc.model
WHERE pc.speed >= '750' INTERSECT
SELECT Product.maker
FROM Product
JOIN Laptop on Product.model = Laptop.model
WHERE Laptop.speed >= '750'

-- 24 --
WITH all_model AS (
SELECT model, price FROM pc
UNION ALL
SELECT model, price FROM printer
UNION ALL
SELECT model, price FROM laptop )
SELECT distinct model
FROM all_model WHERE price = ALL ( SELECT max(price) FROM all_model)

-- 25 --
SELECT DISTINCT Product.maker
FROM Product
JOIN Pc on Product.model = Pc.model
WHERE Pc.ram = (SELECT MIN(ram)
FROM Pc) AND Pc.speed = (SELECT MAX(speed)
FROM Pc
WHERE ram = (SELECT MIN(ram)
FROM pc)) and Product.maker IN (SELECT maker
FROM Product
WHERE type = 'printer')

-- 26 --
SELECT AVG(price)
FROM (SELECT price
FROM PC
WHERE model IN (SELECT model
FROM product
WHERE maker='A' AND type='PC')
UNION ALL SELECT price
FROM Laptop
WHERE model IN (SELECT model
FROM product
WHERE maker='A' AND
type='Laptop')
) as p

-- 28 --
SELECT COUNT(maker)
FROM (SELECT DISTINCT maker
FROM product GROUP BY maker HAVING COUNT(model) = 1) as p

-- 30 --
SELECT point, date, SUM(sum_out), SUM(sum_inc)
FROM( select point, date, SUM(inc) as sum_inc, null as sum_out
FROM Income GROUP BY point, date
UNION
SELECT point, date, null as sum_inc, SUM(out) AS sum_out
FROM Outcome Group by point, date ) as t
GROUP BY point, date ORDER BY point

-- 31 --
SELECT class, country
FROM classes
WHERE bore >= 16

-- 33 --
SELECT ship
FROM outcomes
WHERE result = 'sunk' AND battle = 'North Atlantic'

-- 34 --
SELECT DISTINCT name
FROM Classes, Ships
WHERE launched >= 1922
AND displacement > 35000
AND type='bb'
AND Ships.class = Classes.class

-- 35 --
Select model, type
FROM Product
WHERE model NOT LIKE '%[^0-9]%' OR model NOT LIKE '%[^a-z]%'

-- 36 --
SELECT DISTINCT Classes.class
FROM Classes
JOIN Outcomes on Classes.class = Outcomes.ship
UNION
SELECT DISTINCT Classes.class
FROM Classes
JOIN Ships ON Classes.class = Ships.class
WHERE Ships.class = Ships.name

-- 37 --
SELECT Class
FROM(SELECT class, name
FROM Ships
UNION
SELECT class, ship
FROM Outcomes
JOIN Classes ON Classes.class = Outcomes.ship) as val
GROUP BY class having count(val.name) = 1

-- 38 --
SELECT country
FROM Classes
WHERE type = 'bb'
INTERSECT
SELECT country
FROM Classes
WHERE type = 'bc'

-- 40 --
SELECT DISTINCT maker, max(type)
FROM product
GROUP BY maker
HAVING COUNT(distinct type) = 1 AND COUNT(model) > 1

package ru.itis.taxi.app;

import ru.itis.taxi.dto.SignUpForm;
import ru.itis.taxi.mappers.Mappers;
import ru.itis.taxi.models.User;
import ru.itis.taxi.repositories.UsersRepository;
import ru.itis.taxi.repositories.UsersRepositoryFilesImpl;
import ru.itis.taxi.services.UsersService;
import ru.itis.taxi.services.UsersServiceImpl;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt", Mappers::fromSignUpForm);
        UsersService usersService = new UsersServiceImpl(usersRepository, Mappers::fromSignUpForm);
        User user1 = new User("Marsel", "Sidikov",
                "sidikov.marsel@gmail.com", "qwerty007");

        usersService.signUp(new SignUpForm(user1.getFirstName(), user1.getLastName(),
                user1.getEmail(), user1.getPassword()));

        User user = usersService.getUser();


        user.setEmail("newEmail@mail.ru");
        usersRepository.update(user);
        User loadUser = usersRepository.findById(user.getId());
        System.out.println(loadUser);
        List<User> loadAll = usersRepository.findAll();
        for(User elem : loadAll){
            System.out.println(elem);
        }
        usersRepository.delete(user);


        int i = 0;
    }
}

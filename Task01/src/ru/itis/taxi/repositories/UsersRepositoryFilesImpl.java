package ru.itis.taxi.repositories;

import ru.itis.taxi.dto.SignUpForm;
import ru.itis.taxi.mappers.Mappers;
import ru.itis.taxi.models.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * 30.06.2022
 * 02. TaxiService
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class UsersRepositoryFilesImpl implements UsersRepository {

    private final String fileName;
    private final Function<SignUpForm, User> toUserMapper;

    private static final Function<User, String> userToString = user ->
            user.getId()
                    + "|" + user.getFirstName()
                    + "|" + user.getLastName()
                    + "|" + user.getEmail()
                    + "|" + user.getPassword();

    public UsersRepositoryFilesImpl(String fileName, Function<SignUpForm, User> toUserMapper) {
        this.fileName = fileName;
        this.toUserMapper = toUserMapper;
    }

    @Override
    public List<User> findAll() {
        List<User> list = new ArrayList<>();
        try(RandomAccessFile raf = new RandomAccessFile(fileName, "rw");){
            String line;
            while ( (line = raf.readLine()) != null ){
                String[] arrReadUser = line.split("\\|");
                    list.add(new User(arrReadUser[0], arrReadUser[1], arrReadUser[2], arrReadUser[3], arrReadUser[4]));
            }
            return list;
        }catch (IOException e){
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void save(User entity) {
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            String userAsString = userToString.apply(entity);
            bufferedWriter.write(userAsString);
            bufferedWriter.newLine();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User entity) {
        try(RandomAccessFile raf = new RandomAccessFile(fileName, "rw");){
            String line;
            long lastWritePos = 0;
            long startPos = 0;
            long endPos = -1;
            boolean flag = true;
            while ( (line = raf.readLine()) != null ){
                String[] arrReadUser = line.split("\\|");
                startPos = raf.getFilePointer();
                if(arrReadUser[0].equals(entity.getId())){
                    startPos = lastWritePos;
                    raf.readLine();
                    endPos = raf.getFilePointer();
                    flag = false;
                    break;
                }
                lastWritePos = startPos;
            }
            if(flag){
                return;
            }

            raf.seek(startPos);
            raf.writeBytes(userToString.apply(entity));
            startPos = raf.getFilePointer();

            byte[] buf = new byte[1024];
            int n;
            while (-1 != (n = raf.read(buf))) {
                raf.seek(startPos);
                raf.write(buf, 0, n);
                endPos += n;
                startPos += n;
                raf.seek(endPos);
            }

            raf.setLength(startPos);

            raf.close();
        }catch (IOException e){
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(User entity) {
        deleteById(entity.getId());
    }

    @Override
    public void deleteById(String id) {
        try(RandomAccessFile raf = new RandomAccessFile(fileName, "rw");){
            String line;
            long lastWritePos = -1;
            long writePos = -1;
            long readPos = -1;
            boolean flag = true;
            while ( (line = raf.readLine()) != null ){
                String[] arrReadUser = line.split("\\|");
                writePos = raf.getFilePointer();
                if(arrReadUser[0].equals(id)){
                    writePos = lastWritePos;
                    raf.readLine();
                    readPos = raf.getFilePointer();
                    flag = false;
                    break;
                }
                lastWritePos = writePos;
            }
            if(flag){
                return;
            }

            byte[] buf = new byte[1024];
            int n;
            while (-1 != (n = raf.read(buf))) {
                raf.seek(writePos);
                raf.write(buf, 0, n);
                readPos += n;
                writePos += n;
                raf.seek(readPos);
            }

            raf.setLength(writePos);
            raf.close();
        }catch (IOException e){
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public User findById(String id) {
        try(RandomAccessFile raf = new RandomAccessFile(fileName, "rw");){
            String line;
            while ( (line = raf.readLine()) != null ){
                String[] arrReadUser = line.split("\\|");
                if(arrReadUser[0].equals(id)){
                    return new User(arrReadUser[0], arrReadUser[1], arrReadUser[2], arrReadUser[3], arrReadUser[4]);
                }
            }
            return null;
        }catch (IOException e){
            throw new IllegalArgumentException(e);
        }
    }
}

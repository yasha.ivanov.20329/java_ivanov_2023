package ru.itis;

import ru.itis.jdbc.SimpleDataSource;
import ru.itis.models.Student;
import ru.itis.repositories.StudentsRepository;
import ru.itis.repositories.StudentsRepositoryJdbcImpl;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("Task04\\resources\\db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcImpl(dataSource);

        System.out.println(studentsRepository.findAll());

        Student testStudent = new Student("Yasha", "Ivanov", 22);
        studentsRepository.save(testStudent);

        System.out.println(studentsRepository.findById(15L));
        System.out.println(studentsRepository.findById(55L));
    }
}
